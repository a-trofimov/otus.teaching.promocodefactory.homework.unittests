﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 07, 9),
                        EndDate = new DateTime(2023, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 50
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(default(Partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsActiveFalse_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 50
            };
            var partner = CreateBasePartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит не закончился
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasLimitInitValue_ResetNumberIssuedPromoCodes()
        {
            var numberIssuedPromoCodes = 0;
            var limitInitValue = 10;

            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 50
            };
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = limitInitValue;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NotHasLimitInitValue_ResetNumberIssuedPromoCodes()
        {
            var numberIssuedPromoCodes = 0;
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 50
            };
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_DisablePreviousLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 0
            };
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            var cancelDate = partner.PartnerLimits.FirstOrDefault();
            cancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitMustBeGreaterThanZero_ReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 0
            };
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                    .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_UpdatePartner_ReturnsCreatedAtAction()
        {
            var numberIssuedPromoCodes = 10;
            // Arrange
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = numberIssuedPromoCodes
            };
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
            .ReturnsAsync(partner);

            var expectedActionName = nameof(_partnersController.GetPartnerLimitAsync);

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var actualLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).Limit;

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
            result.Should().BeOfType<CreatedAtActionResult>();
            var createdAtAction = (CreatedAtActionResult)result;
            createdAtAction.ActionName.Should().Be(expectedActionName);
            createdAtAction.RouteValues["id"].Should().Be(partner.Id);
            actualLimit.Should().Be(numberIssuedPromoCodes);
        }
    }
}